const express = require("express");
const app = express();
const connectToDatabase = require("./config/database");
const userRoutes = require("./routes/userRoutes");
const postRoutes = require("./routes/postRoutes");
const voteRoutes = require("./routes/voteRoutes");
const commentRoutes = require("./routes/commentRoutes");
app.use(express.json());

// Rute untuk posts
app.use("/api/posts", postRoutes);

// Route for users
app.use("/api/users", userRoutes);

// Route for votes
app.use("/api/vote", voteRoutes);

// Route for comments
app.use("/api/comment", commentRoutes);

// Route for images
// app.use("/api/images", postRoutes);

const port = 3000;
app.listen(port, () => {
  console.log(`Server Running on Port: ${port}`);
});

connectToDatabase();
require("dotenv").config();
