const mongoose = require('mongoose');

const connectToDatabase = async () => {
    try {
        await mongoose.connect('mongodb://localhost:27017/TestCaseThreads');
        console.log('Database Connected Successfully');
    } catch (error) {
        console.log('Database cannot be connected');
    }
};

mongoose.set('debug', true);

module.exports = connectToDatabase;
