const Comment = require('../models/commentModel');
const User = require("../models/userModel");
const Post = require('../models/postModel');
const moment = require('moment-timezone');

exports.createComment = async (req, res) => {
    try {
        const { content } = req.body;
        const postId = req.query.postId;
        const userId = req.user.id;

        const post = await Post.findById(postId);
        if (!post) {
            return res.status(404).json({ error: 'Postingan tidak ditemukan' });
        }

        const comment = new Comment({
            content: content,
            postID: postId,
            userID: userId
        });

        const savedComment = await comment.save();
        await Post.findByIdAndUpdate(postId, { $push: { comments: savedComment._id } });

        res.status(201).json(savedComment);
    } catch (error) {
        res.status(500).json({ error: error.message });
    }
};

// Mendapatkan semua komentar
exports.getAllComments = async (req, res) => {
    try {
        const comments = await Comment.find({ isDeleted: false });

        if (comments.length === 0) {
            return res.status(404).json({ message: "Komentar tidak ditemukan" });
        }

        const commentsWithUserInfo = await Promise.all(comments.map(async (comment) => {
            const newDeletedAt = moment(comment.deletedAt).tz('Asia/Jakarta').format('YYYY-MM-DD HH:mm:ss');
            const newCreated = moment(comment.createdAt).tz('Asia/Jakarta').format('YYYY-MM-DD HH:mm:ss');
            const newUpdated = moment(comment.updatedAt).tz('Asia/Jakarta').format('YYYY-MM-DD HH:mm:ss');
            const userInfo = await User.findOne({ _id: comment.userID }).select('id username');


            return {
                comment_id: comment._id,
                content: comment.content,
                isDeleted: comment.isDeleted,
                deletedAt: newDeletedAt,
                createdAt: newCreated,
                updatedAt: newUpdated,
                postID: comment.postID,
                user: userInfo,
            };
        }));

        res.status(200).json(commentsWithUserInfo);
    } catch (error) {
        console.error("Error getting all comments:", error.message);
        res.status(500).json({ error: "Terjadi kesalahan saat mengambil semua komentar" });
    }
};

// Mendapatkan komentar berdasarkan ID
exports.getCommentById = async (req, res) => {
    try {
        const commentId = req.params.id;
        const comment = await Comment.findById(commentId);
        if (!comment) {
            return res.status(404).json({ message: "Komentar tidak ditemukan" });
        }
  
        const newDeletedAt = moment(comment.deletedAt).tz('Asia/Jakarta').format('YYYY-MM-DD HH:mm:ss');
        const newCreated = moment(comment.createdAt).tz('Asia/Jakarta').format('YYYY-MM-DD HH:mm:ss');
        const newUpdated = moment(comment.updatedAt).tz('Asia/Jakarta').format('YYYY-MM-DD HH:mm:ss');
        const userInfo = await User.findOne({ _id: comment.userID }).select('id username');
  
        const commentWithUserInfo = {
            comment_id: comment._id,
            content: comment.content,
            isDeleted: comment.isDeleted,
            deletedAt: newDeletedAt,
            createdAt: newCreated,
            updatedAt: newUpdated,
            postID: comment.postID,
            user: userInfo,
        };
  
        res.status(200).json(commentWithUserInfo);
    } catch (error) {
        console.error("Error getting comment by ID:", error.message);
        res.status(500).json({ error: "Terjadi kesalahan saat mengambil komentar" });
    }
  };

// Mengedit komentar berdasarkan ID
exports.updateCommentById = async (req, res) => {
    try {
        const commentId = req.params.id;
        const { content } = req.body;
        const userId = req.user.id;

        const comment = await Comment.findById(commentId);

        if (!comment || comment.userID.toString() !== userId) {
            return res
              .status(403)
              .json({
                error: "Anda tidak memiliki izin untuk memperbarui komentar ini",
              });
          }

        // Temukan komentar berdasarkan ID
        if (!comment) {
            return res.status(404).json({ message: 'Komentar tidak ditemukan' });
        }

        const updatedComment = await Comment.findByIdAndUpdate(
            commentId,
            { content },
            { new: true }
        );

        res.status(200).json(updatedComment);
    } catch (error) {
        console.error('Error updating comment by ID:', error.message);
        res.status(500).json({ error: 'Terjadi kesalahan saat memperbarui komentar' });
    }
};


// Menghapus komentar berdasarkan ID
exports.deleteCommentById = async (req, res) => {
    try {
        const commentId = req.params.id;
        const userId = req.user.id;
        const comment = await Comment.findById(commentId);
        if (!comment || comment.userID.toString() !== userId) {
            return res
              .status(403)
              .json({
                error: "Anda tidak memiliki izin untuk menghapus komentar ini",
              });
          }
        const deletedComment = await Comment.findByIdAndUpdate(
            commentId,
            { isDeleted: true, deletedAt: new Date() },
            { new: true });
        if (!deletedComment) {
            return res.status(404).json({ message: 'Komentar tidak ditemukan' });
        }
        res.status(200).json({ message: 'Komentar berhasil dihapus' });
    } catch (error) {
        console.error('Error deleting comment by ID:', error.message);
        res.status(500).json({ error: 'Terjadi kesalahan saat menghapus komentar' });
    }
};
