const User = require("../models/userModel");
const Post = require("../models/postModel");
const Vote = require("../models/voteModel");
const Comment = require("../models/commentModel");
const fs = require('fs');

const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");

  // Mendapatkan semua pengguna
  exports.getAllUsers = async (req, res) => {
    try {
      const users = await User.find();
      res.status(200).json(users);
    } catch (error) {
      res.status(500).json({ error: error.message });
    }
  };

  // Mendapatkan pengguna berdasarkan ID
  exports.getUserById = async (req, res) => {
    try {
      const userId = req.params.id;
      const user = await User.findById(userId);
      if (user) {
        res.status(200).json(user);
      } else {
        res.status(404).json({ message: "User not found" });
      }
    } catch (error) {
      res.status(500).json({ error: error.message });
    }
  };

  // Membuat pengguna baru
  exports.createUser = async (req, res) => {
    try {
      const { username, email, password, confirmPassword } = req.body;

      // Pemeriksaan konfirmasi kata sandi
      if (password !== confirmPassword) {
        return res
          .status(400)
          .json({ error: "Konfirmasi kata sandi tidak sesuai" });
      }

      // Hash kata sandi sebelum menyimpannya
      const hashedPassword = await bcrypt.hash(password, 10);

      // Lanjutkan dengan membuat pengguna
      const newUser = await User.create({
        username,
        email,
        password: hashedPassword,
      });
      res.status(201).json(newUser);
    } catch (error) {
      res.status(500).json({ error: error.message });
    }
  };

  // Memperbarui informasi pengguna berdasarkan ID
  exports.updateUserById = async (req, res) => {
    try {
      const userId = req.params.id;
      const { username, email, password, confirmPassword } = req.body;

      // Periksa apakah konfirmasi kata sandi sesuai
      if (password !== confirmPassword) {
        return res
          .status(400)
          .json({ error: "Konfirmasi kata sandi tidak sesuai" });
      }

      // Hash kata sandi baru sebelum melakukan pembaruan
      const hashedPassword = await bcrypt.hash(password, 10);

      // Lakukan pembaruan pengguna dengan menggunakan kata sandi yang di-hash
      const updatedUser = await User.findByIdAndUpdate(
        userId,
        { username, email, password: hashedPassword },
        { new: true }
      );

      // Periksa apakah pengguna ditemukan dan kirim respons yang sesuai
      if (updatedUser) {
        res.status(200).json(updatedUser);
      } else {
        res.status(404).json({ message: "Pengguna tidak ditemukan" });
      }
    } catch (error) {
      res.status(500).json({ error: error.message });
    }
  };

  // Menghapus pengguna berdasarkan ID
  exports.deleteUserById = async (req, res) => {
    try {
        const currentUserId = req.user.id;
        const userIdToDelete = req.params.id;

        if (currentUserId !== userIdToDelete) {
            return res.status(403).json({
                error: "Anda tidak memiliki izin untuk menghapus akun ini",
            });
        }

        const userPosts = await Post.find({ userID: userIdToDelete });

        for (const post of userPosts) {
            const postImages = post.images;
            postImages.forEach(image => {
                fs.unlinkSync(image);
            });
        }

        await Post.deleteMany({ userID: userIdToDelete });
        await Vote.deleteMany({ userID: userIdToDelete });
        await Comment.deleteMany({ userID: userIdToDelete })

        const deletedUser = await User.findByIdAndDelete(userIdToDelete);
        if (deletedUser) {
            res.status(200).json({ message: 'User berhasil dihapus' });
        } else {
            res.status(404).json({ message: "User not found" });
        }
    } catch (error) {
        res.status(500).json({ error: error.message });
    }
};


  exports.loginUser = async (req, res) => {
    try {
      const { email, password } = req.body;

      const user = await User.findOne({ email });

      if (!user) {
        return res.status(404).json({ error: "User not found" });
      }

      const isPasswordValid = await bcrypt.compare(password, user.password);

      if (!isPasswordValid) {
        return res.status(401).json({ error: "Wrong Password" });
      }

      const token = jwt.sign({ userId: user.id }, process.env.JWT_SECRET, {
        expiresIn: "1h",
      });

      res.json({ token });
      
    } catch (error) {
      console.error("Error during login:", error.message);
      res.status(500).json({ error: "Terjadi kesalahan saat login" });
    }
  };


