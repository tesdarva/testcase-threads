const Vote = require('../models/voteModel');
const Post = require('../models/postModel');

exports.addVote = async (req, res) => {
    try {
      const { postId } = req.query; 
      const userID = req.user.id;
      
      const existingVote = await Vote.findOne({ userID, postID: postId });
      if (existingVote) {
        await Vote.findByIdAndDelete(existingVote._id);

        await Post.findOneAndUpdate({ _id: postId }, { $pull: { likeID: existingVote._id } });
        return res.status(200).json({ message: 'Disliked' });
      } else {
        const newVote = await Vote.create({ userID, postID: postId });
        await Post.findOneAndUpdate({ _id: postId }, { $push: { likeID: newVote._id } });
        return res.status(200).json({ message: 'Liked' });
      }
    } catch (err) {
      console.error('Error:', err.message);
      return res.status(500).json({ error: 'Terjadi kesalahan saat memproses permintaan' });
    }
};

// Mendapatkan semua suka
exports.getAllVotes = async (req, res) => {
  try {
      const votes = await Vote.find();
      res.status(200).json(votes);
  } catch (error) {
      console.error('Error getting all votes:', error.message);
      res.status(500).json({ error: 'Terjadi kesalahan saat mengambil semua suka' });
  }
};

// Mendapatkan suka berdasarkan ID
exports.getVoteById = async (req, res) => {
  try {
      const voteId = req.params.id;
      const vote = await Vote.findById(voteId);
      if (!vote) {
          return res.status(404).json({ message: 'Suka tidak ditemukan' });
      }
      res.status(200).json(vote);
  } catch (error) {
      console.error('Error getting vote by ID:', error.message);
      res.status(500).json({ error: 'Terjadi kesalahan saat mengambil suka' });
  }
};


