const Post = require("../models/postModel");
const Like = require("../models/voteModel");
const User = require("../models/userModel");
const Comment = require("../models/commentModel");
const moment = require('moment-timezone');
const fs = require('fs');

// Membuat postingan baru
exports.createPost = async (req, res) => {
  try {
    const { title, content } = req.body;
    const userId = req.user.id;
    const images = req.files.map(file => file.path)
    const newPost = await Post.create({ title, content, userID: userId, images });
    res.status(201).json(newPost);
  } catch (error) {
    console.error("Error creating post:", error.message);
    res.status(500).json({ error: "An error occurred while creating the post" });
  }
};

// Mendapatkan semua postingan
exports.getAllPosts = async (req, res) => {
  try {
    const posts = await Post.find({ isDeleted: false, isDeleted: false });
    const postsWithLikeCount = await Promise.all(posts.map(async (post) => {
      const likeCount = await Like.countDocuments({ postID: post._id });
      const newDeletedAt = moment(post.deletedAt).tz('Asia/Jakarta').format('YYYY-MM-DD HH:mm:ss');
      const newCreated = moment(post.createdAt).tz('Asia/Jakarta').format('YYYY-MM-DD HH:mm:ss');
      const newUpdate = moment(post.updatedAt).tz('Asia/Jakarta').format('YYYY-MM-DD HH:mm:ss');
      const userInfo = await User.findOne({ _id: post.userID }).select('id username');
      const comments = await Comment.countDocuments({ postID: post._id });

      return {
        post_id: post._id,
        title: post.title,
        content: post.content,
        isDeleted: post.isDeleted,
        deletedAt: newDeletedAt,
        likes: likeCount,
        Comments: comments,
        createdAt: newCreated,
        updatedAt: newUpdate,
        userID: userInfo,
        images: post.images,
      };
    }));

    res.status(200).json(postsWithLikeCount);
  } catch (error) {
    console.error("Error getting all posts:", error.message);
    res.status(500).json({ error: "Terjadi kesalahan saat mengambil semua postingan" });
  }
};

// Mendapatkan postingan berdasarkan ID
exports.getPostById = async (req, res) => {
  try {
    const postId = req.params.id;
    const post = await Post.findById(postId);
    if (post) {
      const likeCount = await Like.countDocuments({ postID: post._id });
      const newDeletedAt = moment(post.deletedAt).tz('Asia/Jakarta').format('YYYY-MM-DD HH:mm:ss');
      const newCreated = moment(post.createdAt).tz('Asia/Jakarta').format('YYYY-MM-DD HH:mm:ss');
      const newUpdate = moment(post.updatedAt).tz('Asia/Jakarta').format('YYYY-MM-DD HH:mm:ss');
      const userInfo = await User.findOne({ _id: post.userID }).select('id username');
      const comments = await Comment.countDocuments({ postID: post._id });

      const postWithDetails = {
        post_id: post._id,
        title: post.title,
        content: post.content,
        isDeleted: post.isDeleted,
        deletedAt: newDeletedAt,
        likes: likeCount,
        Comments: comments,
        createdAt: newCreated,
        updatedAt: newUpdate,
        userID: userInfo,
        images: post.images,
      };

      res.status(200).json(postWithDetails);
    } else {
      res.status(404).json({ message: "Post not found" });
    }
  } catch (error) {
    console.error("Error getting post by ID:", error.message);
    res.status(500).json({ error: "Terjadi kesalahan saat mengambil postingan" });
  }
};

// Update postingan berdasarkan ID
exports.updatePostById = async (req, res) => {
  try {
    const postId = req.params.id;
    const { title, content } = req.body;

    const userId = req.user.id;
    const post = await Post.findById(postId);

    if (!post || post.userID.toString() !== userId) {
      return res.status(403).json({
        error: "Anda tidak memiliki izin untuk memperbarui postingan ini",
      });
    }

    // Mendapatkan gambar-gambar yang baru diunggah
    let images = [];
    if (req.files && req.files.length > 0) {
      images = req.files.map(file => file.path);
    }

    // Mendapatkan gambar-gambar yang sudah ada sebelumnya
    const existingImages = post.images;

    // Menghapus gambar-gambar yang sudah ada sebelumnya dari sistem file
    existingImages.forEach(image => {
      fs.unlinkSync(image); // Hapus file dari sistem file
    });

    const updatedPost = await Post.findByIdAndUpdate(
      postId,
      { title, content, images },
      { new: true }
    );

    res.status(200).json(updatedPost);
  } catch (error) {
    console.error("Error updating post by ID:", error.message);
    res.status(500).json({ error: "Terjadi kesalahan saat memperbarui postingan" });
  }
};



// Menghapus postingan berdasarkan ID
exports.deletePostById = async (req, res) => {
  try {
    const userId = req.user.id;
    const postId = req.params.id;
    const post = await Post.findById(postId);
    if (!post || post.userID.toString() !== userId) {
      return res.status(403)
        .json({
          error: "Anda tidak memiliki izin untuk menghapus postingan ini",
        });
    }
    const deletedPost = await Post.findByIdAndUpdate(
      postId,
      { isDeleted: true, deletedAt: new Date() },
      { new: true }
    );
    if (deletedPost) {
      res.status(200).json(deletedPost);
    } else {
      res.status(404).json({ message: "Post not found" });
    }
  } catch (error) {
    console.error("Error deleting post by ID:", error.message);
    res.status(500).json({ error: "An error occurred while deleting the post" });
  }
};