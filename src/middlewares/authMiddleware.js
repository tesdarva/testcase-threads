const jwt = require('jsonwebtoken');

module.exports = (req, res, next) => {
    const token = req.headers.authorization;

    if (!token) {
        return res.status(401).send('Unauthorized');
    }

    try {
        const decodedToken = jwt.verify(token.split(' ')[1], process.env.JWT_SECRET);
        req.user = { id: decodedToken.userId };
        next();
    } catch (err) {
        res.status(401).json({ error: 'Invalid token' });
    }
};



