  const mongoose = require('mongoose');

  const postinganSchema = new mongoose.Schema({
    title: {
      type: String,
      required: true
    },
    content: {
      type: String,
      required: true
    },
    isDeleted: {
      type: Boolean,
      default: false
    },
    deletedAt: {
      type: Date,
      default: null
    },
    userID: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'User',
      required: true
    },
    comments: [{
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Comment'
    }],
    likeID: [{
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Vote'
    }],
    images: [{
      type: String,
      required: true
    }],
  }, { timestamps: true });

  const Post = mongoose.model('Post', postinganSchema);

  module.exports = Post;
    