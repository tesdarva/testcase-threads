const mongoose = require('mongoose');

const voteSchema = new mongoose.Schema({
  postID: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Post',
    required: true
  },
  userID: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
    required: true
  }
});

const Vote = mongoose.model('Vote', voteSchema);

module.exports = Vote;
