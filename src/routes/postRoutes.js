const express = require('express');
const router = express.Router();
const authMiddleware = require('../middlewares/authMiddleware');
const PostController = require("../controllers/postController");
const multer = require('multer');

const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'uploads/')
    },
    filename: function (req, file, cb) {
        cb(null, Date.now() +  '-' + file.originalname)
    }
});

// Filter untuk memastikan hanya file gambar yang diizinkan
const imageFilter = function (req, file, cb) {
    if (!file.originalname.match(/\.(jpg|jpeg|png|gif)$/)) {
      return cb(new Error('Only image files are allowed!'), false);
    }
    cb(null, true);
  };

const upload = multer({ storage:storage, fileFilter:imageFilter });

router.use(authMiddleware);
router.get('/', PostController.getAllPosts);
router.get('/:id', PostController.getPostById);
router.post('/', upload.array('images', 1), PostController.createPost);
router.put('/:id', upload.array('images', 1), PostController.updatePostById);
router.delete('/:id', PostController.deletePostById);

module.exports = router;
