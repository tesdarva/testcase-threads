const express = require('express');
const router = express.Router();
const authMiddleware = require('../middlewares/authMiddleware');
const commentController = require('../controllers/commentController');

router.use(authMiddleware);
router.post('/', commentController.createComment);
router.get('/:id', commentController.getCommentById);
router.put('/:id', commentController.updateCommentById);
router.get('/', commentController.getAllComments);
router.delete('/:id', commentController.deleteCommentById);

module.exports = router;
