const express = require('express');
const router = express.Router();
const authMiddleware = require('../middlewares/authMiddleware');
const voteController = require('../controllers/voteController');

// Middleware otentikasi
router.use(authMiddleware);
router.post('/', voteController.addVote);
router.get('/', voteController.getAllVotes);
router.get('/:id', voteController.getVoteById);

module.exports = router;
