const express = require("express");
const router = express.Router();
const authMiddleware = require("../middlewares/authMiddleware");
const UserController = require("../controllers/userController");

router.post("/", UserController.createUser);
router.post("/login", UserController.loginUser);

// Need login
router.use(authMiddleware);
router.get("/", UserController.getAllUsers);
router.get("/:id", UserController.getUserById);
router.put("/:id", UserController.updateUserById);
router.delete("/:id", UserController.deleteUserById);


module.exports = router;
