### Aplikasi Threads

Aplikasi Threads adalah platform untuk berbagi postingan dan komentar antar pengguna. Dengan fitur-fitur yang intuitif dan mudah digunakan, Threads memungkinkan pengguna untuk terlibat dalam berbagai diskusi dan berbagi pemikiran mereka.

### Fitur Utama:

- **Manajemen Pengguna**: Registrasi, login, dan pengelolaan akun pengguna dengan akses yang aman dan terenkripsi.
- **Manajemen Postingan**: Buat, baca, perbarui, dan hapus postingan dengan mudah.
- **Manajemen Komentar**: Tambahkan, lihat, perbarui, dan hapus komentar untuk berpartisipasi dalam diskusi.
- **Manajemen Suara**: Suka pada postingan favorit Anda.

### Persiapan Awal:
1. Pastikan MongoDB sudah berjalan.
2. Jalankan perintah npm install untuk menginstal semua dependensi.
3. Jalankan perintah npm run testcase untuk menjalankan pengujian.
4. Buka aplikasi Postman lalu lihat bagian **Cara Menggunakan** di bawah ini.

### Cara Menggunakan:
1. **Registrasi Pengguna**: Buat akun baru dengan informasi yang diperlukan.
2. **Login Pengguna**: Masuk ke akun Anda dengan email dan kata sandi yang telah didaftarkan.
3. **Kelola Postingan**: Buat postingan baru, baca postingan lainnya, dan interaksi dengan komentar.
4. **Interaksi Komentar**: Tambahkan komentar pada postingan dan berpartisipasi dalam diskusi.
5. **Manajemen Akun**: Perbarui informasi akun Anda dan kelola postingan serta komentar yang telah Anda buat.

### Perhatian:

1. Jika menghapus akun, like dan komentar yang terkait dengan akun tersebut akan ikut terhapus secara otomatis.
2. Hanya pembuat postingan yang dapat mengupdate dan menghapus postingan serta komentar. Jika ingin melakukan perubahan terhadap like atau komentar, pastikan untuk menggunakan akun yang sesuai.

### Kontak:

Jika Anda memiliki pertanyaan atau masukan, jangan ragu untuk menghubungi email saya di darva99801@gmail.com. atau instagram @darvaaph_

### Terima kasih telah menggunakan Threads!
